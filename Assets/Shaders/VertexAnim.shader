﻿Shader "Custom/VertexAnim" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_PosTexture("Position Texture (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Amount("Amount", Range(0,100)) = 1.0
		_speed("Speed", Float) = 0.33
		_boundingMax("Bounding Max", Float) = 1.0
		_boundingMin("Bounding Min", Float) = 1.0
		_numOfFrames("Number Of Frames", int) = 240
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf Standard vertex:vert fullforwardshadows

#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _PosTexture;

	struct Input {
		float2 uv_MainTex;
	};

	half _Glossiness;
	half _Metallic;
	fixed4 _Color;
	float _Amount;
	uniform float _boundingMax;
	uniform float _boundingMin;
	uniform float _speed;
	uniform int _numOfFrames;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = c.a;
	}

	void vert(inout appdata_full v) {
		float timeInFrames = ((ceil(frac(-_Time.y * _speed) * _numOfFrames)) / _numOfFrames) + (1.0 / _numOfFrames);
		float4 texturePos = tex2Dlod(_PosTexture, float4(v.texcoord.x, (timeInFrames + v.texcoord.y), 0, 0));

		v.vertex.xyz += texturePos.xyz;
	}

	ENDCG
	}
		FallBack "Diffuse"
}