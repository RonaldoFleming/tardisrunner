﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class ListAllTextures
{
    public class TextureInfo
    {
        public string _path;
        public int _width;
        public int _height;
        public int _pixelCount;

        public TextureInfo(string path, int width, int height, int pixelCount)
        {
            _path = path;
            _width = width;
            _height = height;
            _pixelCount = pixelCount;
        }
    }

    [MenuItem("EditorTools/ListAllTextures")]
    private static void NewMenuOption()
    {
        List<string> guidsList = new List<string>();
        guidsList.AddRange(AssetDatabase.FindAssets("t:texture2D"));
        List<TextureInfo> textureInfoList = new List<TextureInfo>();

        foreach(string guid in guidsList)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            Texture texture = AssetDatabase.LoadAssetAtPath(path, typeof(Texture)) as Texture;
            textureInfoList.Add(new TextureInfo(path, texture.width, texture.height, texture.width * texture.height));
        }

        textureInfoList.Sort(SortByPixelAmount);

        foreach(TextureInfo info in textureInfoList)
        {
            Debug.Log(info._path + " - " + info._width + "x" + info._height);
        }
    }

    static int SortByPixelAmount(TextureInfo texture1, TextureInfo texture2)
    {
        return texture1._pixelCount.CompareTo(texture2._pixelCount);
    }
}