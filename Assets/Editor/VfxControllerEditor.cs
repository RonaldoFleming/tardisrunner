﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(VfxController))]
public class VfxControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        VfxController myTarget = (VfxController)target;

        myTarget._tardisExplosionPrefab = EditorGUILayout.ObjectField("Tardis Explosion Prefab", myTarget._tardisExplosionPrefab, typeof(GameObject), false) as GameObject;

        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("Not int use: ", "for demonstration purposes only.");
        myTarget._floatRange = EditorGUILayout.Slider("Foat Range", myTarget._floatRange, 0f, 1f);

        myTarget._effectConfiguration = EditorGUILayout.ObjectField("Effect Configuration", myTarget._effectConfiguration, typeof(EffectConfiguration), false) as EffectConfiguration;
    }
}
