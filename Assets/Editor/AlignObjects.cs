﻿using UnityEngine;
using UnityEditor;

public class AlignObjects
{
    [MenuItem("EditorTools/AlignObjects")]
    private static void NewMenuOption()
    {
        float _currentXPos = 0f;
        float _xIncrement = 5f;

        foreach(GameObject selectedObject in Selection.objects)
        {
            selectedObject.transform.position = new Vector3(_currentXPos, 0f, 0f);
            _currentXPos += _xIncrement;
        }
    }
}