﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Not in use, for demonstration purposes only
[CreateAssetMenu(fileName = "Effect", menuName = "EffectConfiguration", order = 1)]
public class EffectConfiguration : ScriptableObject
{
    public GameObject _prefab;
    public float _playDelay;
}
