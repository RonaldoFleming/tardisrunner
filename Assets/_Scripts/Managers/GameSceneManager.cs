﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : SingletonMonoBehaviour<GameSceneManager>
{
    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        EventBus.GameLoadingShown += GameLoadingShown;
        EventBus.MainMenuLoadingShown += MainMenuLoadingShown;
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        EventBus.GameLoadingShown -= GameLoadingShown;
        EventBus.MainMenuLoadingShown -= MainMenuLoadingShown;
    }

    private void GameLoadingShown(object sender, EventArgs e)
    {
        SceneManager.LoadSceneAsync("Game");
    }

    private void MainMenuLoadingShown(object sender, EventArgs e)
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "Game")
        {
            EventBus.OnGameSceneLoaded(this);
        }
        else if(scene.name == "MainMenu")
        {
            EventBus.OnMainMenuSceneLoaded(this);
        }
    }
}
