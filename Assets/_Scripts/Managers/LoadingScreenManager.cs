﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreenManager : SingletonMonoBehaviour<LoadingScreenManager>
{
    [SerializeField]
    private Animator _loadingPanelAnimator = default;
    [SerializeField]
    private AnimationClip _fadeClip = default;

    private LoadingType _currentLoadingType;

    public enum LoadingType
    {
        Game,
        MainMenu,
        Ai
    }

    private void Start()
    {
        EventBus.PlayButtonTouched += PlayButtonTouched;
        EventBus.WatchButtonTouched += WatchButtonTouched;
        EventBus.GameSceneLoaded += GameSceneLoaded;
        EventBus.QuitGameplayButtonTouched += QuitGameplayButtonTouched;
        EventBus.MainMenuSceneLoaded += MainMenuSceneLoaded;
    }

    private void OnDestroy()
    {
        EventBus.PlayButtonTouched -= PlayButtonTouched;
        EventBus.WatchButtonTouched -= WatchButtonTouched;
        EventBus.GameSceneLoaded -= GameSceneLoaded;
        EventBus.QuitGameplayButtonTouched -= QuitGameplayButtonTouched;
        EventBus.MainMenuSceneLoaded -= MainMenuSceneLoaded;
    }

    private void PlayButtonTouched(object sender, System.EventArgs e)
    {
        _currentLoadingType = LoadingType.Game;
        StartCoroutine(ShowLoading());
    }

    private void WatchButtonTouched(object sender, System.EventArgs e)
    {
        _currentLoadingType = LoadingType.Ai;
        StartCoroutine(ShowLoading());
    }

    private void GameSceneLoaded(object sender, System.EventArgs e)
    {
        StartCoroutine(HideLoading());
    }

    private void QuitGameplayButtonTouched(object sender, EventArgs e)
    {
        _currentLoadingType = LoadingType.MainMenu;
        StartCoroutine(ShowLoading());
    }

    private void MainMenuSceneLoaded(object sender, EventArgs e)
    {
        StartCoroutine(HideLoading());
    }

    public float FadeTime {
        get { return _fadeClip.length; }
    }

    private IEnumerator ShowLoading()
    {
        _loadingPanelAnimator.gameObject.SetActive(true);
        _loadingPanelAnimator.SetTrigger("FadeIn");

        yield return new WaitForSeconds(FadeTime);

        if(_currentLoadingType == LoadingType.Game || _currentLoadingType == LoadingType.Ai)
        {
            EventBus.OnGameLoadingShown(this);
        }
        else if(_currentLoadingType == LoadingType.MainMenu)
        {
            EventBus.OnMainMenuLoadingShown(this);
        }

    }

    private IEnumerator HideLoading()
    {
        _loadingPanelAnimator.SetTrigger("FadeOut");

        yield return new WaitForSeconds(FadeTime);

        _loadingPanelAnimator.gameObject.SetActive(false);

        if(_currentLoadingType == LoadingType.Game)
        {
            EventBus.OnGameLoadingDone(this);
        }
        else if(_currentLoadingType == LoadingType.Ai)
        {
            EventBus.OnAiLoadingDone(this);
        }
    }
}
