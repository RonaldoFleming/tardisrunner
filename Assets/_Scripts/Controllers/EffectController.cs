﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectController : MonoBehaviour
{
    public ParticleSystem _particle;

	void Start ()
    {
        _particle.Play();
        StartCoroutine(StartDestroyCountdown(_particle.main.duration));
	}

    private IEnumerator StartDestroyCountdown(float delay)
    {
        yield return new WaitForSeconds(delay);

        Destroy(gameObject);
    }
}
