﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHUDController : MonoBehaviour
{
    [Header("Countdown")]
    public GameObject _countdownPanel;
    public Text _countdownText;
    [Header("Time")]
    public GameObject _timePanel;
    public Text _timeText;
    [Header("Death")]
    public GameObject _deathPanel;
    public Text _bestTimeText;
    public Text _currentTimeText;
    public float _deathPanelDelay = 1f;

    private float _initialTime;
    private float _currentTime = 0f;
    private float _bestTime = 0f;
    private bool _timerBegan = false;

    private bool _aiMode = false;

    private const string bestTimeSaveKey = "BEST_TIME";

    void Start ()
    {
        EventBus.GameLoadingDone += GameLoadingDone;
        EventBus.AiLoadingDone += AiLoadingDone;
        EventBus.GameBegan += GameBegan;
        EventBus.VehicleExploded += VehicleExploded;

        if(PlayerPrefs.HasKey(bestTimeSaveKey))
        {
            _bestTime = PlayerPrefs.GetFloat(bestTimeSaveKey);
        }
	}

    private void Update()
    {
        if(_timerBegan)
        {
            _currentTime = Time.time - _initialTime;
            _timeText.text = ConvertFloatTimeToString(_currentTime);
        }
    }

    private void OnDestroy()
    {
        EventBus.GameLoadingDone -= GameLoadingDone;
        EventBus.AiLoadingDone -= AiLoadingDone;
        EventBus.GameBegan -= GameBegan;
        EventBus.VehicleExploded -= VehicleExploded;
    }

    private void GameLoadingDone(object sender, System.EventArgs e)
    {
        StartCoroutine(StartCountdown());
    }

    private void AiLoadingDone(object sender, System.EventArgs e)
    {
        _aiMode = true;
        StartCoroutine(StartCountdown());
    }

    private void GameBegan(object sender, System.EventArgs e)
    {
        _initialTime = Time.time;
        _currentTime = _initialTime;
        _timeText.text = ConvertFloatTimeToString(_currentTime);
        _timePanel.SetActive(true);
        _timerBegan = true;
    }

    private void VehicleExploded(object sender, System.EventArgs e)
    {
        _timerBegan = false;

        if (_currentTime > _bestTime && !_aiMode)
        {
            _bestTime = _currentTime;
            PlayerPrefs.SetFloat(bestTimeSaveKey, _bestTime);
            PlayerPrefs.Save();
        }

        StartCoroutine(ShowDeathPanel());
    }

    private IEnumerator StartCountdown()
    {
        _countdownPanel.SetActive(true);

        yield return new WaitForSeconds(1f);

        _countdownText.text = "2";

        yield return new WaitForSeconds(1f);

        _countdownText.text = "1";

        yield return new WaitForSeconds(1f);

        _countdownText.text = "GO!";
        EventBus.OnGameBegan(this);

        yield return new WaitForSeconds(1f);

        _countdownPanel.SetActive(false);
    }

    private IEnumerator ShowDeathPanel()
    {
        yield return new WaitForSeconds(_deathPanelDelay);

        _bestTimeText.text = "Best - " + ConvertFloatTimeToString(_bestTime);
        _currentTimeText.text = "Current - " + ConvertFloatTimeToString(_currentTime);
        _deathPanel.SetActive(true);
    }

    public void RetryTouched()
    {
        EventBus.OnPlayButtonTouched(this);
    }

    public void QuitTouched()
    {
        EventBus.OnQuitGameplayButtonTouched(this);
    }

    public string ConvertFloatTimeToString(float time)
    {
        int minutes = Mathf.FloorToInt(time / 60F);
        int seconds = Mathf.FloorToInt(time - minutes * 60);
        return string.Format("{0:0}:{1:00}", minutes, seconds);
    }
}
