﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _popUpsList = default;
    [SerializeField]
    private GameObject _quitPanel = default;

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            ShowQuitPanel();
        }
    }

    private void HideAllPopUps()
    {
        foreach(GameObject popUp in _popUpsList)
        {
            if(popUp.activeSelf)
            {
                popUp.SetActive(false);
            }
        }
    }

    public void Play()
    {
        EventBus.OnPlayButtonTouched(this);
    }

    public void Watch()
    {
        EventBus.OnWatchButtonTouched(this);
    }

    private void ShowQuitPanel()
    {
        HideAllPopUps();
        _quitPanel.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
