﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    public Transform _meshTransform;
    public float _initialMovementSpeed = 20f;
    public float _speedIncreaseRate = 10f;
    public bool _useVertexColor = false;

    private float _movementSpeed;
    private int _speedUpCount = 0;
    private List<Vector3> _rotationAxisList;
    private Vector3 _chosenRotationAxis;

    public int SpeedUpCount {
        get { return _speedUpCount; }
        set { _speedUpCount = value; }
    }

    private void Awake()
    {
        if(_meshTransform != null)
        {
            _rotationAxisList = new List<Vector3>();
            _rotationAxisList.Add(_meshTransform.up);
            _rotationAxisList.Add(-_meshTransform.up);
            _rotationAxisList.Add(_meshTransform.forward);
            _rotationAxisList.Add(-_meshTransform.forward);
            _rotationAxisList.Add(_meshTransform.right);
            _rotationAxisList.Add(-_meshTransform.right);
        }
    }

    private void OnEnable()
    {
        EventBus.GameSpeedIncreased += GameSpeedIncreased;
        _movementSpeed = _initialMovementSpeed + _speedUpCount * _speedIncreaseRate;

        if(_meshTransform != null)
        {
            _chosenRotationAxis = _rotationAxisList[Random.Range(0, _rotationAxisList.Count)];
        }

        if(_useVertexColor)
        {
            Mesh mesh = _meshTransform.gameObject.GetComponent<MeshFilter>().mesh;
            List<Color> newColorsList = new List<Color>();

            foreach(Vector3 vertex in mesh.vertices)
            {
                newColorsList.Add(Random.ColorHSV());
            }

            mesh.SetColors(newColorsList);
        }
    }

    private void OnDisable()
    {
        EventBus.GameSpeedIncreased -= GameSpeedIncreased;
    }

    private void Update()
    {
        gameObject.transform.Translate(new Vector3(0f, 0f, -(_movementSpeed * Time.deltaTime)));
        if(_meshTransform != null)
        {
            _meshTransform.Rotate(_chosenRotationAxis, 180f * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "AsteroidDeathZone")
        {
            gameObject.SetActive(false);
        }
        else if(other.gameObject.tag == "Tardis")
        {
            gameObject.SetActive(false);
            EventBus.OnVehicleCollided(this);
        }
    }

    private void GameSpeedIncreased(object sender, System.EventArgs e)
    {
        _movementSpeed += _speedIncreaseRate;
    }
}
