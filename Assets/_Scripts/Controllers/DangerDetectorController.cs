﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerDetectorController : MonoBehaviour
{
    private List<int> dangers;

    private void Start()
    {
        dangers = new List<int>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Asteroid")
        {
            if(other.transform.position.x < 0)
            {
                dangers.Add(0);
            }
            else if(other.transform.position.x > 0)
            {
                dangers.Add(2);
            }
            else
            {
                dangers.Add(1);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Asteroid" && dangers.Count > 0)
        {
            List<int> safePlaces = new List<int>();

            for (int i = 0; i < 3; i++)
            {
                if(!dangers.Contains(i))
                {
                    safePlaces.Add(i);
                }
            }

            EventBus.OnDangerDetected(this, safePlaces);
            dangers.Clear();
        }
    }
}
