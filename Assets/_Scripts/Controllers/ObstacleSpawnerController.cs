﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawnerController : MonoBehaviour
{
    public List<Transform> _spawnPointTransforms;
    public float _initialSpawnInterval = 4f;
    public float _spawnIntervalDecreaseRate = 0.5f;
    public float _speedIncreaseInterval = 30f;

    private ObjectPooler _objectPooler;
    private float _spawnInterval;
    private int _speedUpCount = 0;
    private bool _canSpawn = false;

	void Start ()
    {
        EventBus.GameBegan += GameBegan;
        EventBus.VehicleExploded += VehicleExploded;

        _objectPooler = gameObject.GetComponent<ObjectPooler>();

        _spawnInterval = _initialSpawnInterval;
	}

    private void OnDestroy()
    {
        EventBus.GameBegan -= GameBegan;
        EventBus.VehicleExploded -= VehicleExploded;
    }

    private void GameBegan(object sender, System.EventArgs e)
    {
        _canSpawn = true;
        StartCoroutine(SpawnObstacleWave());
        StartCoroutine(IncreaseSpeed());
    }

    private void VehicleExploded(object sender, System.EventArgs e)
    {
        _canSpawn = false;
    }

    private IEnumerator SpawnObstacleWave()
    {
        int amountToSpawn = Random.Range(1, _spawnPointTransforms.Count);

        if(amountToSpawn > 0)
        {
            _spawnPointTransforms.Shuffle();

            for(int i = 0; i < amountToSpawn; i++)
            {
                GameObject go = _objectPooler.ShuffleAndGetPooledObject("Asteroid");
                if (go != null)
                {
                    go.transform.position = _spawnPointTransforms[i].position;
                    go.GetComponent<ObstacleController>().SpeedUpCount = _speedUpCount;
                    go.SetActive(true);
                }
            }
        }

        yield return new WaitForSeconds(_spawnInterval);

        if(_canSpawn)
        {
            StartCoroutine(SpawnObstacleWave());
        }
    }

    private IEnumerator IncreaseSpeed()
    {
        yield return new WaitForSeconds(_speedIncreaseInterval);

        _spawnInterval -= _spawnIntervalDecreaseRate;
        _speedUpCount++;
        EventBus.OnGameSpeedIncreased(this);
        if(_canSpawn)
        {
            StartCoroutine(IncreaseSpeed());
        }
    }
}
