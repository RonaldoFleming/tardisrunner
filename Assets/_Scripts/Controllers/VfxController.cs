﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxController : MonoBehaviour
{
    public GameObject _tardisExplosionPrefab;

    //Not in use, for demonstration purposes only
    public float _floatRange;
    public EffectConfiguration _effectConfiguration;
    ////

    private void Start()
    {
        EventBus.VehicleExploded += VehicleExploded;
    }

    private void OnDestroy()
    {
        EventBus.VehicleExploded -= VehicleExploded;
    }

    private void VehicleExploded(object sender, System.EventArgs e)
    {
        EventBus.VehicleExplodedEventArgs args = e as EventBus.VehicleExplodedEventArgs;

        GameObject go = Instantiate(_tardisExplosionPrefab);
        go.transform.position = args.ExplosionPosition;
    }
}
