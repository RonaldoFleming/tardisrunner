﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleController : MonoBehaviour
{
    public float _strafeSpeed = 0.5f;
    public List<float> _lanePositionsList;

    private int _currentLane = 1;
    private bool _canMove = false;
    private float _movementIterator = 0f;

    private bool _aiMode = false;
    private int _aiDesiredLane = 1;

    private void Start()
    {
        EventBus.GameBegan += GameBegan;
        EventBus.AiLoadingDone += AiLoadingDone;
        EventBus.SwipeLeft += SwipeLeft;
        EventBus.SwipeRight += SwipeRight;
        EventBus.VehicleCollided += VehicleCollided;
        EventBus.DangerDetected += DangerDetected;
    }

    private void FixedUpdate()
    {
        if(_aiMode)
        {
            if(_aiDesiredLane < _currentLane && _canMove)
            {
                MoveLeft();
            }
            else if(_aiDesiredLane > _currentLane && _canMove)
            {
                MoveRight();
            }
        }
    }

    private void OnDestroy()
    {
        EventBus.GameBegan -= GameBegan;
        EventBus.AiLoadingDone -= AiLoadingDone;
        EventBus.SwipeLeft -= SwipeLeft;
        EventBus.SwipeRight -= SwipeRight;
        EventBus.VehicleCollided -= VehicleCollided;
        EventBus.DangerDetected -= DangerDetected;
    }

    private void GameBegan(object sender, System.EventArgs e)
    {
        _canMove = true;
    }

    private void AiLoadingDone(object sender, System.EventArgs e)
    {
        _aiMode = true;
    }

    private void SwipeLeft(object sender, System.EventArgs e)
    {
        if(_currentLane > 0 && _canMove && !_aiMode)
        {
            MoveLeft();
        }
    }

    private void SwipeRight(object sender, System.EventArgs e)
    {
        if(_currentLane < 2 && _canMove && !_aiMode)
        {
            MoveRight();
        }
    }

    private void MoveLeft()
    {
        _currentLane--;
        StartCoroutine(Move());
    }

    private void MoveRight()
    {
        _currentLane++;
        StartCoroutine(Move());
    }

    private IEnumerator Move()
    {
        _canMove = false;
        _movementIterator = 0f;
        Vector3 initialPosition = transform.position;
        Vector3 desiredPosition = initialPosition;
        desiredPosition.x = _lanePositionsList[_currentLane];

        while(_movementIterator != 1f)
        {
            _movementIterator = Mathf.Clamp(_movementIterator + _strafeSpeed * Time.deltaTime, 0f, 1f);
            transform.position = Vector3.Lerp(initialPosition, desiredPosition, _movementIterator);
            yield return null;
        }

        _canMove = true;
    }

    private void VehicleCollided(object sender, System.EventArgs e)
    {
        EventBus.OnVehicleExploded(this, transform.position + new Vector3(0f, 3f, 0f));
        gameObject.SetActive(false);
    }

    private void DangerDetected(object sender, System.EventArgs e)
    {
        EventBus.DangerDetectedEventArgs args = e as EventBus.DangerDetectedEventArgs;

        if (!args.SafePositions.Contains(_currentLane))
        {
            if (args.SafePositions.Count > 1)
            {
                if(_currentLane == 1)
                {
                    int chosenIndex = Random.Range(0, args.SafePositions.Count);
                    _aiDesiredLane = args.SafePositions[chosenIndex];
                }
                else
                {
                    _aiDesiredLane = 1;
                }
            }
            else
            {
                _aiDesiredLane = args.SafePositions[0];
            }
        }
    }
}
